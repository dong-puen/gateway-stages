package com.fir.nacos.service;


import com.fir.nacos.entity.LoginResultDTO;

/**
 * @author fir
 * @date 2023/4/23 17:03
 */
public interface IAuthService {


    /**
     * 用户统一登录
     *
     * @param username 账号
     * @param password 密码
     * @return 登录信息
     */
    LoginResultDTO login(String username, String password);
}


