import Router from 'vue-router'
import Vue from "vue";
import loginTest from "@/views/loginTest.vue";

Vue.use(Router)

const routes = [
  {
    path: '/',
    name: 'login',
    component: loginTest
  },
]


const router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: routes
})

export default router
