package com.fir.nacos.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;


/**
 * @author fir
 * @date 2023/7/28 17:53
 */
@Data
@Component
@ConfigurationProperties(prefix = "global")
public class GlobalConfig {

    /**
     * 令牌校验默认过期时间
     */
    private Integer timeNum;

    /**
     * 令牌头变量名称
     */
    private String tokenHeader;


    /**
     * 设置每次登录的过期时间单位
     */
    public TimeUnit timeUnit = TimeUnit.MINUTES;
}
