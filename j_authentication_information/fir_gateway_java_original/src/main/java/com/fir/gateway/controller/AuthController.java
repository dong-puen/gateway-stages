package com.fir.gateway.controller;

import com.fir.gateway.config.result.AjaxResult;
import com.fir.gateway.dto.ConnectDTO;
import com.fir.gateway.service.IAuthService;
import com.fir.gateway.utils.RSAUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;


/**
 * 系统登录验证
 *
 * @author dpe
 * @date 2022/8/4 22:58
 */
@Api(tags = "系统登录接口")
@Slf4j
@RestController
@RefreshScope
public class AuthController {

    /**
     * 系统登录验证 接口层
     */
    @Resource
    private IAuthService iAuthService;

    @ApiOperation("客户端与服务端建立连接")
    @RequestMapping("/k")
    public AjaxResult info() {
        String publicKey = iAuthService.getPublicKey();
        return AjaxResult.success(publicKey);
    }


    @ApiOperation("客户端与服务端建立连接")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "k", value = "公钥字符串"),
            @ApiImplicitParam(name = "k", value = "客户端公钥字符串"),
    })
    @RequestMapping("/cn")
    public AjaxResult connect(String k, String ck) {
        ConnectDTO connectDTO = iAuthService.info(k, ck);
        String content = RSAUtils.encryptSection(connectDTO, connectDTO.getClientPublicKey());
        return AjaxResult.success(content);
    }
}
