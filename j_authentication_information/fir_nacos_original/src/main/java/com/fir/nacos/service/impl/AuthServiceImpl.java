package com.fir.nacos.service.impl;

import com.alibaba.fastjson2.JSONObject;
import com.alibaba.nacos.common.utils.StringUtils;
import com.fir.nacos.config.GlobalConfig;
import com.fir.nacos.entity.ConnectDTO;
import com.fir.nacos.entity.LoginResultDTO;
import com.fir.nacos.service.IAuthService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;


/**
 * @author fir
 * @date 2023/4/23 17:03
 */
@Slf4j
@Service
public class AuthServiceImpl implements IAuthService {


    @Resource
    private GlobalConfig globalConfig;

    @Resource
    private RedisTemplate<Object,Object> redisTemplate;


    /**
     * 用户统一登录
     *
     * @param username 账号
     * @param password 密码
     * @return 登录信息
     */
    @Override
    public LoginResultDTO login(String username, String password){
        LoginResultDTO loginResultDTO = new LoginResultDTO();
        if(StringUtils.isNoneBlank(username) && StringUtils.isNoneBlank(password)){
            // 此处简单处理，根据实际的项目做更改
            if(username.equals("fir") && password.equals("123456")){


                // 生成 Token (可用多种方式例如jwt,此处不额外集成)
                String token = "Bearer " + username + "token";

                ConnectDTO connectDTO = ConnectDTO.builder()
                        .name(username)
                        .build();

                Object o = JSONObject.toJSONString(connectDTO);
                redisTemplate.opsForValue().set(token, o, globalConfig.getTimeNum(), globalConfig.timeUnit);


                loginResultDTO = LoginResultDTO.builder()
                        .token(token)
                        .build();
            }
        }

        return loginResultDTO;
    }
}
