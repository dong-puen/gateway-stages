#### 原始

```

## 原始的vue2基础项目,未安装任何东西
fir_vue2_original
```



#### 原始的项目场景

最简单的配置，实现了Nacos配置、服务注册、网关转发微服务接口。

```
fir_gateway_java_original

fir_nacos_original
```

#### 全局异常捕捉

在原始项目的基础之上，实现全局异常捕捉，404处理，统一返回对象。

```
h_exception/
	fir_gateway_java_original
	fir_nacos_original
	nacos配置
```

#### 网关与令牌

网关集成redis与令牌校验器

微服务集成redis,AjaxResult,swagger,登录接口。

```
i_token/
	fir_gateway_java_original
	fir_nacos_original
	nacos配置
```

#### 能与网关通信的vue2项目

增加了网关的跨域配置，取消了微服务的跨域配置。

增加了一个能连接后端的vue项目。

vue有向后端发送请求，携带请求头的功能。

```
i_vue_token/
	fir_gateway_java_original
	fir_nacos_original
	fir_security_vue2_original
	nacos配置
```

#### 前端后公私钥与认证信息

增加了前后端的公私钥交换，以及一些认证信息，加密密钥，会话id，盐。

```
j_authentication_information/
	fir_gateway_java_original
	fir_nacos_original
	fir_security_vue2_original
	nacos配置
```

#### 前端后整体加解密

增加了前端(fir_security_vue2_original)后端(fir_gateway_java_original)的整体加解密拦截器

```
k_encryption_decryption/
	fir_gateway_java_original
	fir_nacos_original
	fir_security_vue2_original
	nacos配置
```

#### 防重放校验拦截器

增加了前端(fir_security_vue2_original)后端(fir_gateway_java_original)的防重放校验拦截器

```
k_replay_attack/
	fir_gateway_java_original
	fir_nacos_original
	fir_security_vue2_original
	nacos配置
```

#### 完整性校验拦截器

增加了前端(fir_security_vue2_original)后端(fir_gateway_java_original)的完整性校验拦截器

```
l_reqIntegrity/
	fir_gateway_java_original
	fir_nacos_original
	fir_security_vue2_original
	nacos配置
```

#### 防XSS拦截器

后端(fir_gateway_java_original)的防XSS拦截器

```
l_xss/
	fir_gateway_java_original
	fir_nacos_original
	fir_security_vue2_original
	nacos配置
```

