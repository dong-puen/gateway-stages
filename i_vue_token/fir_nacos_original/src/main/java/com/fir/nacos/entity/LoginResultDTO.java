package com.fir.nacos.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


/**
 * 用户的登录接口信息
 *
 * @author fir
 * @date 2023/4/23 17:57
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class LoginResultDTO {


    /**
     * token令牌
     */
    private String token;
}
