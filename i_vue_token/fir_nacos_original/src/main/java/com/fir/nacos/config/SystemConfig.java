package com.fir.nacos.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

/**
 * @author fir
 * @date 2023/5/4 14:24
 */
@Data
@Component
@ConfigurationProperties(prefix = "system")
public class SystemConfig {


    /**
     * 设置每次登录的过期时间
     */
    public final static Integer time = 30;


    /**
     * 设置每次登录的过期时间单位
     * TimeUnit.MINUTES(分钟)
     */
    public final static TimeUnit TIME_UNIT = TimeUnit.MINUTES;
}
