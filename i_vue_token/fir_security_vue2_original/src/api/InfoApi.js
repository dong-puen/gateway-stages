import {dataInterface} from "@/utils/request";

export default {

    /** 系统登陆接口 **/
    login(obj) {
        return dataInterface("/tick/auth/login","get", obj)
    },

    oneGetValue(obj){
        return dataInterface("/tick/getValue", "get", obj)
    },
}