/** 全局变量配置-start **/

// url白名单设置
const whiteList = [
    "/tick/auth/login",
]

/** 全局变量配置-end **/



export default {


    /**
     * 读取信息
     */
    get(key) {
        return sessionStorage.getItem(key)
    },
    
    
    /**
     * 添加信息
     */
    set(key, value) {
        sessionStorage.setItem(key, value)
    },


    /**
     * 登录之后进行处理
     */
    loginDeal(token){
        this.set("token", token)
    },



    /**
     * gateway网关验证信息处理(请求头)
     */
    gatewayRequest(config) {
        let key = true;
        whiteList.find(function (value) {
            if (value === config.url) {
                key = false;
            }
        });

        // 对非白名单请求进行处理
        if (key) {
            // 请求体数据
            let token = this.get("token")

            // 请求中增加token
            if (token) {
                config.headers.Authorization = token;
            }
        }

        return config;
    },

}