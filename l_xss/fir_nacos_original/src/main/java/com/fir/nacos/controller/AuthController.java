package com.fir.nacos.controller;

import com.alibaba.nacos.common.utils.StringUtils;
import com.fir.nacos.config.result.AjaxResult;
import com.fir.nacos.config.result.AjaxStatus;
import com.fir.nacos.entity.LoginResultDTO;
import com.fir.nacos.service.IAuthService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import javax.annotation.Resource;


/**
 * @author fir
 */
@Slf4j
@Api(tags = "系统接口")
@RestController
@RefreshScope
@RequestMapping("/auth")
public class AuthController {


    @Resource
    private IAuthService iAuthService;

    /**
     * 用户统一登陆
     *
     * @param username 用户
     * @param password 密码
     * @return 登录信息/登录失败信息
     */
    @ApiOperation("登录接口")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "username", value = "用户"),
            @ApiImplicitParam(name = "password", value = "密码")
    })
    @RequestMapping("/login")
    public AjaxResult login(String username, String password) {
        LoginResultDTO loginResultDTO;
        if(StringUtils.isNoneBlank(username) && StringUtils.isNoneBlank(password)){
                loginResultDTO = iAuthService.login(username, password);
        }else {
            return AjaxResult.error(AjaxStatus.NULL_LOGIN_DATA);
        }
        return AjaxResult.success(loginResultDTO);
    }
}