package com.fir.nacos.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.format.FormatterRegistry;
import org.springframework.web.servlet.config.annotation.*;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.*;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;
import java.util.ArrayList;
import java.util.List;


/**
 * mvc配置
 * @author dpe
 */
@Configuration
@EnableSwagger2
@EnableWebMvc
public class MvcConfig implements WebMvcConfigurer  {

    @Value("${swagger.enable}")
    private Boolean swaggerEnable;

    private static final String PACKAGE_NAME = "com.fir.nacos.controller";


    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**")
                .allowedOriginPatterns("*")
                .allowedMethods("GET","HEAD","POST","DELETE","OPTIONS")
                .allowCredentials(true)
                .maxAge(3600)
                .allowedHeaders("*");
    }


    @Override
    public void addFormatters(FormatterRegistry registry) {
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {

    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {

        registry.addResourceHandler("/**").addResourceLocations(
                "classpath:/static/");
        registry.addResourceHandler("swagger-ui.html").addResourceLocations(
                "classpath:/META-INF/resources/");
        registry.addResourceHandler("/webjars/**").addResourceLocations(
                "classpath:/META-INF/resources/webjars/");
        WebMvcConfigurer.super.addResourceHandlers(registry);
    }


    @Bean
    public Docket createRestApi() {
        return selectDocket();
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder().title("API文档").version("1.0").build();
    }


    /**
     * 设置swagger生成模式
     * @return swagger配置
     */
    private Docket selectDocket(){
        Docket docket;

        docket = tokenDocket();
        return docket;
    }


    /**
     * 每个接口都具有token, swagger配置
     */
    private Docket tokenDocket(){
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .enable(swaggerEnable)
                .globalOperationParameters(jwtToken())
                .select()
                .apis(RequestHandlerSelectors.basePackage(PACKAGE_NAME))
                .paths(PathSelectors.any())
                .build();
    }


    /**
     * 接口header的token
     */
    private List<Parameter> jwtToken() {
        String jwt = "123";

        ParameterBuilder tokenPar = new ParameterBuilder();
        List<Parameter> pars = new ArrayList<>();
        // 声明 key
        tokenPar.name("Authorization")
                // 文字说明
                .description("jwt-token令牌")
                // 类型为字符串
                .modelRef(new ModelRef("string"))
                // 参数形式为 header 参数
                .parameterType("header")
                // 默认值
                .defaultValue(jwt)
                // 是否必须
                .required(false);
        pars.add(tokenPar.build());

        return pars;
    }
}