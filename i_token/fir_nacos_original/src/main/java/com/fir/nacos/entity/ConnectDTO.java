package com.fir.nacos.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;


/**
 * @author fir
 * @date 2023/4/23 17:57
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value="用户登录密钥信息")
public class ConnectDTO implements Serializable {

    private static final long serialVersionUID = -1;

    @ApiModelProperty("用户名称")
    private String name;
}
