package com.fir.gateway;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;

import javax.annotation.Resource;



@SpringBootTest
public class RedisTest {



    @Resource
    private RedisTemplate<String,Object> redisTemplate;


    @Test
    void set(){

        redisTemplate.opsForValue().set("name123", "value123");
    }

    @Test
    void get(){

        Object object = redisTemplate.opsForValue().get("name123");


        System.out.println(object);
    }
}
