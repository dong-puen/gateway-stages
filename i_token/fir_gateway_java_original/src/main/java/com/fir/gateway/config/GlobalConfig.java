package com.fir.gateway.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.List;


/**
 * @author fir
 * @date 2023/7/28 17:53
 */
@Data
@Component
@ConfigurationProperties(prefix = "global")
public class GlobalConfig {

    /**
     * 全局异常捕捉-打印堆栈异常
     */
    private boolean printStackTrace;

    /**
     * 令牌头变量名称
     */
    private String tokenHeader;

    /**
     * 令牌校验
     */
    private boolean tokenCheck;


    /**
     * 白名单路由-不进行网关校验直接放过
     */
    private List<String> whiteUrls;

}
